#IsItUp

Nothing fancy but a simple script to check 
other server(s) or url(s) using GET method 
and sends an email when attempts reach 
MAX_FAILED_ATTEMPTS.

Clone/download and run 
```
npm i && npm run prod
```

Don't forget to add your own .env file!