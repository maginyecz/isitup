#!/usr/bin/env node
'use strict'
const dotenv = require('dotenv').config()
const app = require('../src/app')
const http = require('http')

const port = process.env.PORT||3000
app.set('port', port)

// catch any other routes
app.use((q,r,n)=>{
	const err = new Error('Not found')
	err.status = 404
	n(err)
})

// Catch errors and respond
app.use((err,q,r,n) => {
	console.error(err ? (new Date()) + ' ' + err.toString() : '')
	if (process.env.NODE_ENV!=='development') {
		delete err.stack
	}
	r.status( err.statusCode||500 ).send(err.toString())
})

const server = http.createServer( app )

server.listen( port )
server.on('error', onError)
server.on('listening', onListening)

function onError(err) {
	if(err.syscall!=='listen') {
		throw err
	}
	const bind = typeof port === 'string' 
		? 'Pipe ' + port
		: 'Port ' + port

	switch (err.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges')
			process.exit(1)
		break
		case 'EADDRINUSE':
			console.error(bind + ' is already in use')
			process.exit(1)
		break
		default:
			throw err
		break
	}
}

function onListening() {
	const addr = server.address()
	const bind = typeof addr === 'string'
		? 'pipe ' + addr
		: 'port ' + addr.port
	console.info('Listening on ' + bind)
}