'use strict'
const {
	MAX_FAILED_ATTEMPTS = 10, 
	MAX_TIMEOUT = 5000,
	SEND_EMAIL = true,
	EMAIL_MANDRILL_API_KEY = 'YourMandrillApiKey',
	EMAIL_SENDER = 'isitup@example.com',
	EMAIL_TO = 'me@example.com',
} = process.env

const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const app = express()
const axios = require('axios')
const nodeMailer = require('nodemailer')
const smtpTransport = require('nodemailer-smtp-transport')
const mandrillTransport = require('nodemailer-mandrill-transport')
const transporter = nodeMailer
										.createTransport( mandrillTransport({
											auth: {
												apiKey: EMAIL_MANDRILL_API_KEY
											}
										}))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))
app.use(helmet())

// TODO: switch to Redis in the future
const Tries = new Map()

// returns an axios request
function checkURL(url, timeout) {
	url = url.indexOf('http://')>-1||url.indexOf('https://')>-1 
				? url 
				: `http://${url}`
	return axios({ method: 'get', url, timeout })
}

// routes
app.get('/', (q,r)=>r.send('HELLO!'))

// /check/http://example.com
app.get('/check/*', (q,r,n) => {
	let url = q.path.split('/check')[1].slice(1)
	checkURL(url, MAX_TIMEOUT)
	.then( res => {
		console.info( `${new Date()} ${res.status} ${url}` )
		return r.status(200).json({ url, status: res.status }) 
	})
	.catch( err => {
		if (!Tries.has(url)) {
			Tries.set(url, 0)
		}
		let nextval = Tries.get(url)
		Tries.set(url, ++nextval)
		if (nextval == MAX_FAILED_ATTEMPTS) {
			console.error('REACHED_MAX_ATTEMPTS')
			if (SEND_EMAIL) {

				transporter.sendMail({
					from: EMAIL_SENDER,
					to: EMAIL_TO,
					subject: `${url} is probably down`,
					text: `Hi, 
I tried to reach '${url}' ${nextval} times 
but the server doesn't respond within ${MAX_TIMEOUT} ms.
Sincerely yours: 
heartbeat`
				}, (err, ok) => {
					if (err) { 
						console.error('sendMail error', err)
					}
				})

			}
			Tries.set(url, 0)
		}
		n(err)
	})
})

// Lists all tries as an object
app.get('/tries', (q,r,n)=> {
	const obj = {}
	Tries.forEach((v,k)=> obj[k] = v)
	r.json( obj ) 
})

module.exports = app